<%-- 
    Document   : index
    Created on : 29-03-2020, 12:34:50
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Seccion</title>
        <link rel="stylesheet" type="text/css" href="view.css" media="all">
        <script type="text/javascript" src="view.js"></script>
    </head>
    <body id="main_body">
        <%-- --%>
        <img id="top" src="top.png" alt="">
	<div id="form_container">
	<h1><a>Registro</a></h1>
            <form name="form" action="controller" method="POST">
                <div class="form_description">
                    <h2>Registro</h2>
                    <p>Registro de Seccion de Alumno</p>
		</div>						
		<ul >
                    <li id="li_1" >
                        <label class="description" for="element_1">Nombre </label>
                        <div>
                            <input id="element_1" name="nombre" class="element text medium" type="text" maxlength="200" value=""/> 
                        </div>
                        <p class="guidelines" id="guide_1">
                        <small>Ingrese Nombre y Apellido</small></p> 
                    </li>		
                    <li id="li_2" >
                        <label class="description" for="element_2">Sección </label>
                        <div>
                            <input id="element_2" name="seccion" class="element text small" type="number" maxlength="3" value=""/> 
                        </div>
                        <p class="guidelines" id="guide_2">
                        <small>Ingrese Sección (Solo Números)</small></p> 
                    </li>
                    <li class="buttons">
                        <input type="hidden" name="form_id" value="103675" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Registrar" />
                    </li>
		</ul>
		</form>	
	</div>
	<img id="bottom" src="bottom.png" alt="">
    </body>
</html>
