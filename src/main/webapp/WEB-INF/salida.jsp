<%-- 
    Document   : salida
    Created on : 29-03-2020, 12:41:07
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Seccion</title>
    </head>
    <body>
        <h1>Registro de Seccion</h1>
        <%
            String nombre = (String) request.getAttribute("nombre"); 
            long seccion = (long) request.getAttribute("seccion");
            String fecha = (String) request.getAttribute("fecha");
            String hora = (String) request.getAttribute("hora");
            
        %>
        <p>Hola <%=nombre%>, tu registro se ha realizado correctamente hoy <%=fecha%> a las <%=hora%></p>
    </body>
</html>
