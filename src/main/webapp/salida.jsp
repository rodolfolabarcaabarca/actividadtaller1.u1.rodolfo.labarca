<%-- 
    Document   : salida
    Created on : 29-03-2020, 12:41:07
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Seccion</title>
        <link rel="stylesheet" type="text/css" href="view.css" media="all">
        <script type="text/javascript" src="view.js"></script>
    </head>
    </head>
    <body id="main_body">
        <%
            String nombre = (String) request.getAttribute("nombre"); 
            String seccion = (String) request.getAttribute("seccion");         
            String fecha = (String) request.getAttribute("fecha");
            String hora = (String) request.getAttribute("hora");            
        %>
        <h1><a>Registro</a></h1>
        <div id="form_container">
            <div class="form_description">
                <h2>Registro</h2>
                <p>Registro de Seccion de Alumno</p>
            </div>
            <ul>
                    <li id="li_1" >
                        <label class="description" for="element_1">Estimado <%=nombre%>, tu registro se ha realizado correctamente en la seccion <%=seccion%>, hoy <%=fecha%> a las <%=hora%></label>
                    </li>
            </ul>
        </div>
        <img id="bottom" src="bottom.png" alt="">
    </body>
</html>
