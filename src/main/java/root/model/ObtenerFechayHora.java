/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author rlabarca
 */
public class ObtenerFechayHora {
    public String fecha(){
        Date fecha = new Date();
        TimeZone timeZone = TimeZone.getTimeZone("America/Santiago");
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        formatoFecha.setTimeZone(timeZone);
        String salidaFecha = formatoFecha.format(fecha); 
        return salidaFecha;
    }

    public String hora(){
        Date hora = new Date();
        TimeZone timeZone = TimeZone.getTimeZone("America/Santiago");
        DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
        formatoHora.setTimeZone(timeZone);
        String salidaHora = formatoHora.format(hora); 
        return salidaHora;
    }   
}